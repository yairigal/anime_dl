"""A setuptools based setup module."""
# io.open is needed for projects that support Python 2.7
# It ensures open() defaults to text mode with universal newlines,
# and accepts an argument to specify the text encoding
# Python 3 only projects can skip this import
from io import open
from os import path

# Always prefer setuptools over distutils
from setuptools import find_packages
from setuptools import setup

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(
    name='anime-dl',  # Required
    version='1.1.0',  # Required
    description='Download anime episodes automation',  # Optional
    long_description=long_description,  # Optional
    url='https://gitlab.com/yairigal/anime_dl',  # Optional
    author='Yair Yigal',  # Optional
    packages=find_packages(),  # Required
    python_requires='>=3.7',
    install_requires=['attrdict==2.0.1',
                      "cached-property==1.5.1",
                      "splinter==0.13.0",
                      "waiting==1.4.1",
                      "requests==2.23.0",
                      "wells==1.5.0",
                      "bs4",
                      "docopt"],
    entry_points={  # Optional
        'console_scripts': [
            'anime-dl = anime_dl.main:main',
        ],
    },
)
