# anime-dl
Download episodes from various of sites and upload it to telegram group.

### Prerequisites
* ffmpeg installed on your pc
* [chromedriver](https://chromedriver.chromium.org/downloads) matching your google chrome version installed and in 
the PATH.
* You should already be authenticated to [telegram web](https://web.telegram.org/) via your main chrome profile.


### Getting started
```bash
$ pip install anime-dl
```

Usage:
```bash
Anime episodes helper.

Usage:
  anime-dl run -c <config> [-t <telegram_config>]
  anime-dl download <save_folder> <from>...
  anime-dl upload [-t <telegram_config>] <telegram_group> <local_file>...
  anime-dl -h | --help

Options:
  -h --help     Show this screen.
```

#### Config structure
```json
{
  "download_folder": "C:\\path\\to\\local\\folder",
  "season": 1,
  "link_list": [
    "",
    "https://www.dailymotion.com/video/x7mfv1b"
],
  "group_name": "Seven deadly sins"
}
```
* ```download_folder``` - where to save the downloaded episodes.
* ```season``` - the season index (used in file names e.g. S1E4)
* ```link_list``` - list of links to download episodes from. Each episode has its own spot on the list (e.g. spot 6 
on the list is numbered as episode 7). To skip a file just insert empty string ("").
* ```group_name``` - the name of the group on telegram web. Its recommended to pin the group to top since the 
webdriver might not find the group if its lower down. 

#### telegram config
```json
{
  "user_data_dir": "C:\\Users\\Yair\\AppData\\Local\\Google\\Chrome\\User Data",
  "headless": false,
  "fullscreen": false,
  "incognito": false
}
```
* ```user_data_dir``` - path to the folder where the chrome user data is stored. necessary to skip login to the 
telegram web site.
* ```headless``` - whether to open the browser or keep it in the background.
* ```fullscreen``` - whether to open the browser in fullscreen mode.
* ```incognito``` - whether to open the browser in incognito mode.

### Supported sites:
* Google drive
* Dailymotion


## Contributions
* Thanks for [sylecn](https://pypi.org/user/sylecn/) for using his [m3u8downloader](https://pypi.org/project/m3u8downloader/) library (with slight changes)