import re
from pathlib import Path

from anime_dl.downloaders.base_downloader import BaseEpisodeDownloader
from anime_dl.downloaders.dailymotion import DailymotionDownloaderBase
from anime_dl.downloaders.google_drive import GoogleDriveDownloaderBase


class EpisodeDownloader(BaseEpisodeDownloader):
    """Main episode downloader.

    This class organizes all the different downloaders to a single class.
    Each downloader is picked based on the download link.
    """
    PATTERNS = {
        "https://www.dailymotion.com/.*": DailymotionDownloaderBase,
        "https://drive.google.com/.*": GoogleDriveDownloaderBase,
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.args = args
        self.kwargs = kwargs

    def downloader(self, link: str) -> BaseEpisodeDownloader:
        """Get the proper downloader based on the download link.

        The downloaders are cached once the are created to keep a single instance per downloader type.

        Args:
            link (str): URL of the episode.

        Returns:
            BaseEpisodeDownloader. the suitable downloader.
        """
        for pattern, downloader in self.PATTERNS.items():
            if re.match(pattern, link) is not None:
                if not hasattr(self, downloader.__name__):
                    downloader_instance = downloader(*self.args, **self.kwargs)
                    setattr(self, downloader.__name__, downloader_instance)

                return getattr(self, downloader.__name__)

    def download(self, link: str, file_path: Path) -> None:
        self.downloader(link).download(link, file_path)

    def file_extension(self, link):
        return self.downloader(link).file_extension(link)
