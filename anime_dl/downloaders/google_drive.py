from pathlib import Path

import requests
from bs4 import BeautifulSoup

from anime_dl.downloaders.base_downloader import BaseEpisodeDownloader


def download_file_from_google_drive(id, destination, logger):
    URL = "https://docs.google.com/uc?export=download"

    with requests.Session() as session:
        response = session.get(URL, params={'id': id}, stream=True)
        token = get_confirm_token(response)

        if token:
            params = {'id': id, 'confirm': token}
            response = session.get(URL, params=params, stream=True)

        save_response_content(response, destination, logger)


def get_confirm_token(response):
    for key, value in response.cookies.items():
        if key.startswith('download_warning'):
            return value


def save_response_content(response, destination, logger):
    CHUNK_SIZE = 32768

    downloaded = 0
    with open(destination, "wb") as f:
        for chunk in response.iter_content(CHUNK_SIZE):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
                downloaded += CHUNK_SIZE
                if downloaded % (10 * (1024 ** 2)) == 0:
                    logger.info(f"\t{downloaded // (1024 ** 2)}MB Downloaded")


class GoogleDriveDownloaderBase(BaseEpisodeDownloader):
    def download(self, link: str, file_path: Path) -> None:
        video_id = max(link.split("/"), key=len)

        self.logger.info(f"Downloading {link} to {file_path}")
        download_file_from_google_drive(video_id, str(file_path), self.logger)

    def file_extension(self, link: str) -> str:
        soup = BeautifulSoup(requests.get(link).content, features="html.parser")
        filename = next(iter(soup.findAll("title"))).string
        filename = filename.replace(" - Google Drive", '')
        extension = filename.split(".")[-1]
        return extension
