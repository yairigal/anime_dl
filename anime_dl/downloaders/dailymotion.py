import json
from pathlib import Path

import requests

from anime_dl.utils.logger import get_logger
from anime_dl.utils.m3u8_downloader_windows import M3u8Downloader
from anime_dl.downloaders.base_downloader import BaseEpisodeDownloader

logger = get_logger(__name__)


class DailymotionDownloaderBase(BaseEpisodeDownloader):
    TEMP_DIR = Path("C:\\temp")
    DOWNLOAD_URL = (
        "https://www.dailymotion.com/player/metadata/video/{video_id}"
        "?embedder=https%3A%2F%2Fwww.dailymotion.com%2Fvideo%2F{video_id}"
        "&referer=https%3A%2F%2Fwww.dailymotion.com%2Fvideo%2F{video_id}"
        "&app=com.dailymotion.neon"
        "&locale=en-US"
        "&client_type=website"
        "&section_type=player"
        "&component_style=_")

    def _get_m3u8_link(self, link: str) -> str:
        video_id = link.split("/")[-1]
        url = self.DOWNLOAD_URL.format(video_id=video_id)
        response = json.loads(requests.get(url).content)
        m3u8_link = response['qualities']['auto'][0]['url']
        return m3u8_link

    def download(self, link: str, local_file_path: Path) -> None:
        m3u8_link = self._get_m3u8_link(link)
        logger.info(f"Downloading {link} to {local_file_path}")
        M3u8Downloader(m3u8_link, str(local_file_path), tempdir=self.TEMP_DIR).start()

    def file_extension(self, link: str) -> str:
        return "mp4"
