import json
import logging
from typing import List
from pathlib import Path

from attrdict import AttrDict
from cached_property import cached_property

from anime_dl.utils.logger import get_logger
from anime_dl.utils.telegram_handler import TelegramHandler


class BaseEpisodeDownloader:
    """Abstract class for episode downloader.

    To implement new downloader, a class should inherit from this class and implement two functions:
    * download - how to download a file (this is changed from site to site).
    * file_extension - what is the extension of the file.

    Attributes:
        telegram_config_path (str): path to the telegram config file.
        config_path (str): path to the config file.
    """

    def __init__(self, config='configs/dr_stone_s1.json', telegram_handler_config='configs/telegram_handler.json'):
        self.telegram_config_path = telegram_handler_config
        self.config_path = config

        # Create download folder if not exists
        download_folder = Path(self.config.download_folder)
        if not download_folder.exists():
            self.logger.debug(f"Creating local folder {download_folder}")
            download_folder.mkdir()

    @cached_property
    def logger(self) -> logging.Logger:
        """class logger."""
        return get_logger(__name__)

    @property
    def telegram_api(self) -> TelegramHandler:
        """Telegram API handler."""
        return TelegramHandler(**self.telegram_config)

    @cached_property
    def telegram_config(self) -> dict:
        """Telegram config."""
        try:
            with open(self.telegram_config_path) as f:
                return json.load(f)

        except:
            return {}

    @cached_property
    def config(self) -> AttrDict:
        """Run configurations."""
        with open(self.config_path) as f:
            return AttrDict(json.load(f))

    @cached_property
    def telegram_group(self) -> str:
        """The telegram group name."""
        return self.config['group_name']

    def download_episodes(self) -> List[Path]:
        """Download episodes from the config link list.

        Returns:
            list. list of downloaded episodes paths.
        """
        episodes = []
        for ep_num, link in enumerate(self.config.link_list):
            if link == '':
                self.logger.warning(f"Skipping episode #{ep_num + 1}")
                continue

            file_name = f"S{self.config.season}E{ep_num + 1}.{self.file_extension(link)}"
            file_path = Path(self.config.download_folder) / file_name

            self.logger.info(f"Starting {file_name} download")
            self.download(link, file_path)
            episodes.append(file_path)

        return episodes

    def download(self, link: str, file_path: Path) -> None:
        """Download an episode from the link.

        Args:
            link (str): URL of the episode.
            file_path (Path): path of the file (include the file name).
        """
        raise NotImplementedError

    def file_extension(self, link: str) -> str:
        """Evaluate the episode file extension.

        Args:
            link (str): URL of the episode.

        Returns:
            str. the extension of the file (e.g. mp4, avi)
        """
        raise NotImplementedError

    def upload(self, episodes: List[Path], telegram_group: str = None) -> None:
        """Upload the episodes to the telegram group.

        Args:
            episodes (list): list of paths to locally saved files.
            telegram_group (str): name of the telegram group tp upload to (optional, if not supplied, the group will
                be read from the config).
        """
        self.logger.info("Started uploading files")
        telegram_group = telegram_group if telegram_group is not None else self.telegram_group
        self.telegram_api.upload_files(group=telegram_group, files=episodes)

    def start(self) -> None:
        """Download all the episodes and upload them to the telegram group."""
        episodes = self.download_episodes()
        self.upload(episodes)
