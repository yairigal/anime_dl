import logging


def get_logger(name):
    logging.basicConfig(format='%(asctime)s %(message)s')
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    return logger