import os
import re
from typing import List
from pathlib import Path

import waiting
import splinter
from cached_property import cached_property
from splinter.driver.webdriver import BaseWebDriver
from splinter.driver.webdriver.chrome import Options
from splinter.driver.webdriver import WebDriverElement
from selenium.common.exceptions import StaleElementReferenceException

from anime_dl.utils.logger import get_logger

HOUR_IN_SECONDS = 60 * 60

logger = get_logger(__name__)


class TelegramHandler:
    """Telegram web API.

    Tools for accessing the web telegram api.

    Attributes:
        opts (splinter.driver.webdriver.chrome.Options): options for chrome web driver.
        headless (bool): whether to open a chrome window.
        fullscreen (bool): if headless is False, whether the chrome windows should take the entire screen or not.
        incognito (bool): whether to open the chrome window in incognito mode.
    """
    WEB_DRIVER = 'chrome'
    SEND_BUTTON_LABEL = 'Send file'
    TELEGRAM_WEB_URL = 'https://web.telegram.org/'

    def __init__(self, user_data_dir=None, headless=False, fullscreen=False, incognito=False):
        self.opts = Options()
        if user_data_dir is not None:
            user_data_dir = Path(user_data_dir)
            if not user_data_dir.exists():
                user_data_dir = os.getenv('LOCALAPPDATA') / user_data_dir
                assert user_data_dir.exists(), f"{user_data_dir} path does not exist"

            self.opts.add_argument(fr'user-data-dir={user_data_dir}')

        self.headless = headless
        self.fullscreen = fullscreen
        self.incognito = incognito

        logger.debug("Opening telegram web")
        self.browser.visit(self.TELEGRAM_WEB_URL)

    @cached_property
    def browser(self) -> BaseWebDriver:
        return splinter.Browser(self.WEB_DRIVER,
                                options=self.opts,
                                headless=self.headless,
                                fullscreen=self.fullscreen,
                                incognito=self.incognito)

    @property
    def upload_button(self) -> WebDriverElement:
        """Return the Upload file button element."""
        return next(el for el in self.browser.find_by_tag("input", wait_time=10)
                    if self.SEND_BUTTON_LABEL in el.outer_html)

    def upload_files(self, group: str, files: List[Path], file_timeout=2 * HOUR_IN_SECONDS) -> None:
        """Upload the files to the specified group.

        Args:
            group (str): the name of the group to upload the files to.
            files (list): tuple of Path objects represent the paths of the files to upload.
        """
        self.browser.find_by_text(group).click()  # Open the group chat
        for local_file in files:
            logger.info(f"Uploading {local_file} to {group}")
            self.upload_button.fill(str(local_file))
            logger.info(f"Waiting {file_timeout} seconds to {local_file.name} to upload")
            waiting.wait(lambda: not self.is_episode_uploading(local_file.name),
                         timeout_seconds=file_timeout,
                         waiting_for=f'{local_file.name} to upload')

    def _is_episode_uploading(self, episode_element: WebDriverElement) -> bool:
        """Check if the episode is currently uploading.

        Help function to check if the element is on upload state.

        Args:
            episode_element (WebDriverElement): the episode HTML element.

        Returns:
            bool whether if the element is on upload state.
        """
        return re.search("\d+\.?\d* of \d+\.?\d* .?B", episode_element.outer_html) is not None

    def is_episode_uploading(self, name: str, timeout=10) -> bool:
        """Check if the episode is currently uploading.

        Help function to check if the element is on upload state.

        Args:
            name (str): the episode name (should be unique).
            timeout (int): timeout to wait for the browser to search for the elements.

        Returns:
            bool whether if the episode is on upload state.
        """
        try:
            # Search for episode element (e.g. where title=S_E_.mp4)
            brothers = [el for el in self.browser.find_by_xpath(f"//*[contains(@title, '{name}')]", wait_time=timeout)]
            assert len(brothers) == 1, f"Found {len(brothers)} elements matching episode {name}"
            brother = brothers[0]

            uploading = [el for el in brother.parent.find_by_tag('span', wait_time=timeout)
                         if self._is_episode_uploading(el)]
            return len(uploading) > 0

        except StaleElementReferenceException:
            return False

    def close(self) -> None:
        if 'browser' in self.__dict__:
            logger.info("Closing browser")
            self.browser.quit()
            del self.__dict__['browser']

    def __del__(self):
        self.close()
