"""Anime episodes helper.

Usage:
  anime-dl run -c <config> [-t <telegram_config>]
  anime-dl download (-c <config> | <save_folder>  <from>...)
  anime-dl upload [-t <telegram_config>] <telegram_group> <local_file> <local_file>...
  anime-dl -h | --help

Options:
  -h --help     Show this screen.
"""
from pathlib import Path

import docopt

from anime_dl.utils.logger import get_logger
from anime_dl.downloaders.downloader import EpisodeDownloader

logger = get_logger(__name__)


def run(downloader, **args):
    downloader.start()


def download(downloader, **args):
    if args['<from>'] and args['<save_folder>']:
        save_folder = Path(args['<save_folder>'])
        logger.info(f"Downloading files to {save_folder}")
        for i, link in enumerate(args['<from>']):
            extension = downloader.file_extension(link)
            logger.info(f"Started downloading {link}")
            downloader.download(link, save_folder / f"EP_{i + 1}.{extension}")
    
    else:
        downloader.download_episodes()
    
    



def upload(downloader, **args):
    logger.info("Uploading...")
    downloader.upload(telegram_group=args['<telegram_group>'],
                      episodes=[Path(local_file) for local_file in args['<local_file>']])


def main():
    args = docopt.docopt(__doc__)

    this_file = Path(__file__)

    # Save configurations
    telegram_config_path = this_file.parent / "configs/telegram_handler.json"
    if args['-t'] and args['<telegram_config>'] is not None:
        telegram_config_path = Path(args['<telegram_config>'])

    config_path = this_file.parent / "configs/config.json"
    if args['-c'] and args['<config>'] is not None:
        config_path = Path(args['<config>'])

    # initiate downloader
    logger.info("Initializing downloader")
    downloader = EpisodeDownloader(config=str(config_path.absolute()),
                                   telegram_handler_config=str(telegram_config_path.absolute()))

    # Start executing command
    if args['run']:
        run(downloader, **args)

    elif args['download']:
        download(downloader, **args)

    elif args['upload']:
        upload(downloader, **args)


if __name__ == '__main__':
    main()
